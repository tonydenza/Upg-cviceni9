﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni9._3d
{
    class Program
    {
        static char[,] Obrazec10(int hrana)
        {
            char[,] obrazec = new char[hrana, hrana];
            int i = 0, j = 0;

            while (i != obrazec.GetLength(0))
            {
                for (j = i; j < obrazec.GetLength(1); j++)
                    obrazec[i, j] = '*';
                i++;
            }

            return obrazec;
        }

        static void Vypis(char[,] obrazec)
        {
            for (int i = 0; i < obrazec.GetLength(0); i++)
            {
                for (int j = 0; j < obrazec.GetLength(1); j++)
                {
                    if (j == 20)
                        Console.WriteLine(obrazec[i, j]);
                    else
                        Console.Write(obrazec[i, j]);
                }
            }
        }

        static void Main(string[] args)
        {
            char[,] obrJ = Obrazec10(21);
            Vypis(obrJ);

            Console.ReadLine();
        }
    }
}
