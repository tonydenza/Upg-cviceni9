﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni9._3c
{
    class Program
    {
        static char[,] Obrazec02(int hrana)
        {
            char[,] obrazec = new char[hrana, hrana];

            for (int i = 0; i < obrazec.GetLength(0); i++)
            {
                for (int j = 0; j < obrazec.GetLength(1); j++)
                {
                    if (i == 0)
                        obrazec[i, j] = '*';
                    else if (j == 0 || j == hrana - 1)
                        obrazec[i, j] = '*';
                    else if (i == hrana - 1)
                        obrazec[i, j] = '*';
                    else if (i == j)
                        obrazec[i, j] = '*';
                    else
                        obrazec[i, j] = ' ';
                }
            }
            return obrazec;
        }

        static void Vypis(char[,] obrazec)
        {
            for (int i = 0; i < obrazec.GetLength(0); i++)
            {
                for (int j = 0; j < obrazec.GetLength(1); j++)
                {
                    if (j == 20)
                        Console.WriteLine(obrazec[i, j]);
                    else
                        Console.Write(obrazec[i, j]);
                }
            }
        }

        static void Main(string[] args)
        {
            char[,] obrC = Obrazec02(21);
            Vypis(obrC);

            Console.ReadLine();
        }
    }
}
