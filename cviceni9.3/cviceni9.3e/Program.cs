﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni9._3d
{
    class Program
    {
        static char[,] Obrazec05(int hrana)
        {
            char[,] obrazec = new char[hrana, hrana];

            for (int i = 0; i < obrazec.GetLength(0); i++)
            {
                for (int j = 0; j < obrazec.GetLength(1); j++)
                {
                    if (i == 0)
                        obrazec[i, j] = '*';
                    else if (j == 0 || j == hrana - 1)
                        obrazec[i, j] = '*';
                    else if (i == hrana - 1)
                        obrazec[i, j] = '*';
                    else if (j == hrana / 2)
                        obrazec[i, j] = '*';
                    else if (i == hrana / 2)
                        obrazec[i, j] = '*';
                    else
                        obrazec[i, j] = ' ';
                }
            }
            return obrazec;
        }

        static void Vypis(char[,] obrazec)
        {
            for (int i = 0; i < obrazec.GetLength(0); i++)
            {
                for (int j = 0; j < obrazec.GetLength(1); j++)
                {
                    if (j == 20)
                        Console.WriteLine(obrazec[i, j]);
                    else
                        Console.Write(obrazec[i, j]);
                }
            }
        }

        static void Main(string[] args)
        {
            char[,] obrE = Obrazec05(21);
            Vypis(obrE);

            Console.ReadLine();
        }
    }
}
